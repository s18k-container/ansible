#!/bin/bash
set -euxo pipefail

image="${CI_REGISTRY_IMAGE}"
tag="${CI_COMMIT_REF_SLUG}"

echo "Building $image:$tag"

ctr=$(buildah from gitlab.com:443/s18k-container/dependency_proxy/containers/alpine:latest)

buildah run $ctr apk add --update --no-cache python3 py3-pip git curl
buildah run $ctr python -m ensurepip --upgrade
buildah run $ctr python -m pip install --upgrade ansible ansible-lint

buildah commit --squash --rm $ctr "$image:$tag"